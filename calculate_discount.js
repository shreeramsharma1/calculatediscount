const fs = require('fs');
// const path = require("path");
// const selectedOfferCode = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/offer_code.json'), 'utf-8'));
// const transaction_detail = JSON.parse(fs.readFileSync(path.join(__dirname, './input_json/transaction_detail.json'), 'utf-8'));
let rateValue ;
let lcyValue ;
let chargesValue ;
let totalAmountValue ;
let orgChargeswithGst ;
let totalDiscountValue ;
function discountapplicable(selectedOfferCode, transaction_detail) {
        rateValue = transaction_detail.ibr-((selectedOfferCode.chargesDiscount.chargeDiscount/transaction_detail.orgCharges)*transaction_detail.buySellSign);
        chargesValue = (transaction_detail.orgCharges-selectedOfferCode.chargesDiscount.chargeDiscount)+(transaction_detail.orgCharges-selectedOfferCode.chargesDiscount.chargeDiscount)*transaction_detail.GST/100 
        lcyValue =  transaction_detail.amount*(rateValue/transaction_detail.perUnit)
        totalAmountValue = lcyValue+chargesValue
        orgChargeswithGst = parseFloat(transaction_detail.orgCharges)+(parseFloat(transaction_detail.orgCharges)*(parseFloat(transaction_detail.GST)/100));    
        totalDiscountValue = totalAmountValue-((parseFloat(transaction_detail.amount)*parseFloat(transaction_detail.rate))+orgChargeswithGst)
        
        let Case1 = {
                "offerdetail": {
                "chargeDetail": {     
                   "chargeDiscountType": selectedOfferCode.chargesDiscount.chargeDiscountType,
                   "chargesDiscount": selectedOfferCode.chargesDiscount.chargeDiscount,
                   "orgCharges": transaction_detail.orgCharges,
                   "charges": `${chargesValue}`
                },
                "rateDetail": {
                        "rateDiscountType": selectedOfferCode.rateDiscount.rateDiscountType,
                        "rateDiscountOn": selectedOfferCode.rateDiscount.rateDiscountOn,
                        "rateDiscountOrMargin": selectedOfferCode.rateDiscount.rateDiscountOrMargin ,
                        "orgRate":  transaction_detail.rate,
                        "rate": rateValue.toString(),
                        "ibr": transaction_detail.ibr,
                        "cardRate": transaction_detail.cardRate 
                },
                "transDetail": {
                        "transTypeCode": transaction_detail.transTypeCode,
                        "currency": transaction_detail.currency,
                        "lcyAmount": lcyValue.toString(),
                        "totalAmount": totalAmountValue.toString(),
                        "totalDiscount": totalDiscountValue.toString()
        }
    },
    "status": "success"
 }
return Case1;
}
// console.log(discountapplicable(selectedOfferCode, transaction_detail));
module.exports = discountapplicable;