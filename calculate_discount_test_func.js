const xlsx = require('xlsx');
const discountapplicable = require('./calculate_discount');

function calculate_discount_test() {
    try{
        const workbook = xlsx.readFile('Rate Engine Test Cases.xlsx');
        const excel_sheet = workbook.Sheets['CD-TestFamily-CalculateDiscount'];

        let expected = [], actual = [];
        
        let transJson, offerJson, expectedOutJson, actualOutJson;

        let rowCount = 0;
        for(let x of Object.keys(excel_sheet)){
            if (x[0] == "I"){
                rowCount += 1;
            }
        }

        let addr;
        for(let i=3; i<=rowCount; i++){
  
            addr = "I"+i;
            transJson = (excel_sheet[addr].v).replace('trnDetail:','');
            transJson = (eval('['+transJson+']'))[0];
  
            addr = "J"+i;
            offerJson = (excel_sheet[addr].v).replace('selectedOffer:','');
            offerJson = (eval('['+offerJson+']'))[0];

            addr = "S"+i;
            expectedOutJson = (excel_sheet[addr].v);
            expectedOutJson = (eval('['+expectedOutJson+']'))[0];
           
            actualOutJson = discountapplicable((offerJson),(transJson));
            
            expected.push(expectedOutJson); 
            actual.push(actualOutJson);
            
            
        }
        return {"Expected":expected,"Actual":actual};
    }
    catch(err){
        console.log(err);
    }
    
};

module.exports = calculate_discount_test;






