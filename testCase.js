const chai = require('chai');
const expect = chai.expect;
const calculate_discount_test = require('./calculate_discount_test_func');
let data = calculate_discount_test();

describe('Testing the Is Code Applicable function',function(){  
    for(let i=0;i<data.Expected.length;i++){
        describe(`Testing : Test case no.${i + 1}`,function(){
            it('Actual JSON output should match with Expected JSON output',function(){
                expect(data.Actual[i]).to.eql(data.Expected[i]);
            })
        });
    }
});
